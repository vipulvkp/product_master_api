Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
   namespace :v1 do
     namespace :sobject do
       namespace :bulk do
          post 'product_master' => 'product_master#create'
          get 'product_master' => 'product_master#index'
          post 'product_alias' => 'product_alias#create'
          get 'product_alias' => 'product_alias#index'
       end
     end
   end
end
