module Error
	class PramataApiError < StandardError
		attr_accessor :pointer

		def response_code
			"500"
		end

		def response
			{
				errors: [
					{
						status: response_code,
						source: source,
						title:  title,
						detail: detail
					}
				]
			}
		end

    private

    def title
      ""
    end

    def detail
      "Please contact support@pramata.com with the URL"
    end

    def source
      { pointer: pointer }
    end

    def log_level
      "warn"
    end

	end
end