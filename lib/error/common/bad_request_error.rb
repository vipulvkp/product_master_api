module Error
	module Common
		class BadRequestError < PramataApiError
			def title
				"Bad Request Error"
			end

			def response_code
				"400"
			end
		end
	end
end