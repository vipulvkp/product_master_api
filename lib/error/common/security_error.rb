module Error
	module Common
		class SecurityError < PramataApiError
			def title
				"Security Error"
			end

			def response_code
				"403"
			end

      def detail
        message
      end
      
		end
	end
end