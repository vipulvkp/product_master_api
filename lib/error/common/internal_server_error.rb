module Error
	module Common
		class InternalServerError < PramataApiError
			def title
				"Internal Server Error"
			end

			def response_code
				"500"
			end
		end
	end
end