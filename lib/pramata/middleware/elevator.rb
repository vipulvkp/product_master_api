require 'apartment/elevators/generic'
require_relative '../json_web_token'

module Pramata
  module Middleware
    class Elevator < Apartment::Elevators::Generic
      def call(*args)
        begin
          super
        rescue Apartment::TenantNotFound => e
          Rails.logger.error "Tenant not found #{e.class} #{e.message} #{e.backtrace}"
          [
            404, 
            { 'Content-Type' => 'application/json'},
            [
              {
                "errors" => [
                  {
                    "status" => "404",
                    "code" => "TENANT_NOT_FOUND",
                    "source" => { "pointer" => "invalid_tenant" },
                    "title" => "Tenant not found",
                    "detail" => "You have passed an invalid tenant, please correct the tenant information in token and try again"
                  }
                ]
              }.to_json
            ]
          ]
        rescue Pramata::InvalidToken => e
          Rails.logger.error "Authentication Failed :: #{e.class} #{e.message} #{e.backtrace}"
          [
            401, 
            { 'Content-Type' => 'application/json'},
            [
              {
                "errors" => [
                  {
                    "status" => "401",
                    "code"  => "UNAUTHORIZED",
                    "source" => { "pointer" => "invalid_token" },
                    "title" =>  "Authentication Failed - Invalid Token",
                    "detail" => "Valid token should be used in http header in order to access api service"
                  }
                ]
              }.to_json
            ]
          ]
        rescue StandardError => e
          Rails.logger.error "Failed to connect API service :: #{e.class} #{e.message} #{e.backtrace}"
          [
            500, 
            { 'Content-Type' => 'application/json'},
            [
              {
                "errors" => [
                  {
                    "status" => "500",
                    "code"  => "FAILED_TO_CONNECT_API_SERVICE",
                    "source" => { "pointer" => "failed_to_connect_api_service" },
                    "title" =>  "Failed to connect API service",
                    "detail" => "Sorry, something went wrong while connecting to the api service, please contact support@pramata.com with the route of this request"
                  }
                ]
              }.to_json
            ]
          ]
        end
      end

      def parse_credentials_from_jwt(request)
        jwt_token = request.get_header('HTTP_PRAMATAJWTTOKEN')
        decoded_token = JsonWebToken.decode(jwt_token)
        credentials = decoded_token["data"]
        return credentials
      end

      def parse_tenant_name(request)
        credentials = parse_credentials_from_jwt(request)
        $tenant = Tenant.find_by_name(credentials["tenant_name"]) if credentials.present?
        $app_user_id = credentials["app_user_id"]
        if $tenant.present?
          $mysql_client = Mysql2::Client.new(
            host: $tenant.database_host,
            username: $tenant.database_username,
            password: $tenant.database_password,
            database: $tenant.database,
            reconnect: true,
            encoding: 'utf8mb4'
          )
          $mysql_client.query_options.merge!(:symbolize_keys => true)
          return $tenant.database 
        else
          raise Apartment::TenantNotFound
        end
      end
    end
  end
end
