module Pramata
  class PramataException
  	def initialize e
  		@e = e
  	end
    def log
      app_user_name = "SystemUser"
      AppException.create(:name=>@e.message[0..250],
                          :message=>@e.message,
                          :stacktrace=>@e.backtrace,
                          :created_by=>app_user_name
                          )
    end
  end
end
