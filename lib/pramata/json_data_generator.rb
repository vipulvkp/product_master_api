module Pramata
	module JsonDataGenerator
		def into_json
			data = Hash.new
      data["type"] = @resource
      data["id"] = @resource_id
      data["attributes"] = @data
			return data
		end
	end
end