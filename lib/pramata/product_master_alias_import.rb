require "csv"
module Pramata
  class ProductMasterAliasImport
    
    def initialize(csv_file_path,params={})
      @csv_file = csv_file_path
      @product_master_hash = ActiveSupport::OrderedHash.new 
      @product_alias_array = []
    end

    def import
      CSV.foreach(@csv_file, headers: true) do |row|
          product_master_info = [row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]]
          guid = (row[0].nil?) ? Digest::MD5.hexdigest(product_master_info.join(",")) : row[0]
          @product_master_hash[guid] = ProductMaster.new(id:3,guid: guid,name:row[1],category1:row[2],category2:row[3],category3:row[4],category4:row[5],category5:row[6],is_asset:row[7],created_by:row[8])
          #@product_master_hash[guid] = [guid,*product_master_info]
          @product_alias_array << ProductAlias.new(product_master_guid:guid,id:row[9],description:row[10],created_by:row[11])
          #@product_alias_array << [guid,row[9],row[10],row[11]]
      end
      ProductMaster.import @product_master_hash.values, on_duplicate_key_update: [:guid,:name,:category1,:category2,:category3,:category4,:category5,:is_asset,:created_by]
      ProductAlias.import @product_alias_array, on_duplicate_key_update: [:product_master_guid,:description,:created_by]
    end
  end
end
