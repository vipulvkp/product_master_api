module Pramata
  module Md5KeyGenerator
    def self.key(k)
      md5 = Digest::MD5.new
      md5.update k
      md5.hexdigest   
    end
  end
end