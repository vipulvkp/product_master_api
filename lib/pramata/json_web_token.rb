require 'jwt'
SECRET = "zIjcp4k_Cn1OVJpTby652Dtgkjdhckjshkjdfhsajkdd0Ix2lDcPVv_2dYRMrTnU5nyMx-lrAh0G6zsUz2UDMhk5gfO9hqzhhrbirx7Yw3Q01pZTOQb6nrle5i_nPqM2QcM-mNUT5PPIzez0I45OvXNRFAtLroyEphsPC39Ur-_TY1VAUzwwdJytTCZZbZW_SlgHlQu0Pt--HlNNOVYE12CDrX4kBVA8GAybuYsLwQ0oGjaF80i-dQRcRkXcireZusKhq2qofejfRtqZFkqvLQ0W6xOILuBdEdG5ZhJjy45ijiJVbM4RmhmcXGR8MGWl7RKbd-BJ1m2HWQyjGeX2RZFq57lzwvgAr5Hx6yrHKZg"
module Pramata
  class InvalidToken < StandardError; end

  class JsonWebToken
    class << self
      JWT_TOKEN_EXPIRE_TIME = 2 # in minutes
      def encode(payload)
        payload[:exp] = JWT_TOKEN_EXPIRE_TIME.minutes.from_now.to_i
        JWT.encode(payload, SECRET)
      end
      
      def decode(token)
        decoded_token = JWT.decode token, SECRET, true, { :algorithm => 'HS256' }
        body = decoded_token[0]
        HashWithIndifferentAccess.new body
      rescue
        raise InvalidToken
      end
    end
  end 
end
