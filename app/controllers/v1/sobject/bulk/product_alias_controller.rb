class V1::Sobject::Bulk::ProductAliasController < ApplicationController

  def create
      @product_alias_array = []
      params[:product_alias_data].each{|pa|
        @product_alias_array << ProductAlias.new(id:pa[0],product_master_guid:pa[1],description:pa[2],created_by:pa[3])
      }
      ApplicationRecord.transaction do
        ProductAlias.import @product_alias_array, on_duplicate_key_update: [:id,:product_master_guid,:description,:created_by]
      end
      render :json=>{:msg=>"Product alias import completed successfully","total_imports"=>@product_alias_array.length}.to_json, :status=>200
  end


  def index

  end

end
