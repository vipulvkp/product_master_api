class V1::Sobject::Bulk::ProductMasterController < ApplicationController

  def create
      @product_master_record = []
      params[:product_master_values].each{|guid,row| 
          @product_master_record << ProductMaster.new(guid: guid,name:row[1],category1:row[2],category2:row[3],category3:row[4],category4:row[5],category5:row[6],is_asset?:row[7],created_by:row[8])
      }
      ApplicationRecord.transaction do
        ProductMaster.import @product_master_record, on_duplicate_key_update: [:guid,:name,:category1,:category2,:category3,:category4,:category5,:is_asset?,:created_by]
      end
      render :json=>{:msg=>"Product Master import completed successfully","total_imports"=>@product_master_record.length}.to_json, :status=>200
    
  end


  def index

  end

end
