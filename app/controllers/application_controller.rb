require "error/common"
require "open3"
class ApplicationController < ActionController::Base
	include Error::Common
	rescue_from StandardError, :with => :render_standard_error
    before_action  :security_check
    skip_before_action :verify_authenticity_token

  def pointer
    request.original_url
  end

  def render_pramata_error(error)
    error.pointer = pointer
    render json: error.response, status: error.response_code
  end

  def render_standard_error(error)
    ise = InternalServerError.new(error.message)
    ise.set_backtrace(error.backtrace)
    render_pramata_error ise
  end

  private

  def security_check
    return true if apartment_switched? && tenant_active? && identifier_table_exists? && identifier_found? && identifier_matched?
    $tenant.deactivate!
    security_error = SecurityError.new(@security_error)
    alert_team
    render_pramata_error security_error
  end

  def apartment_switched?
    return true if ActiveRecord::Base.connection_config[:database] == $tenant.database
    @security_error = "Tenant switching did not work"
    return false
  end

  def tenant_active?
    return true if $tenant.active?
    @security_error = "Tenant not active"
    return false
  end

  def identifier_table_exists?
    return true if ActiveRecord::Base.connection.table_exists?(TenantIdentifier.table_name)
    @security_error = "Identifier table doesn't exist in database"
    return false
  end

  def identifier_found?
    return true if TenantIdentifier.first.present?
    @security_error = "Identifier key not found"
    return false
  end

  def identifier_matched?
    return true if $tenant.identifier == TenantIdentifier.first.key
    @security_error = "Identifier did not match"
    return false
  end

  def alert_team
    subject = "Critical Alert!!! - Platform down for tenant - " + $tenant.name
    Open3.capture2("mutt","-s",subject,"--","thor-dev@pramata.com", :stdin_data=> @security_error)
  end

  def get_additional_email_body
    body = {}
    body[:request_url] = request.url
    body[:request_params] = params
    body[:username] = $user[:username] if $user.present?
    body.to_json
  end

  def get_additional_email_subject
    subject = {}
    subject[:tenant_name] = $tenant.name if $tenant.present?
    subject.to_json
  end   

end
