class Tenant < ApplicationRecord
  def db_configuration
    {
      adapter: 'mysql2',
      encoding: 'utf8mb4',
      host: database_host,
      database: database,
      username: database_username,
      password: database_password
    }
  end

  def active?
    is_active
  end

  def deactivate!
    update_attribute(:is_active, false)
  end

end
